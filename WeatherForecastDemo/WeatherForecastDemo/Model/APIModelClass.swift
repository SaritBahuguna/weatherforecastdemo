//
//  APIModelClass.swift
//  WeatherForecastDemo
//
//  Created by Mac  on 10/10/18.
//  Copyright © 2018 SaritBahuguna. All rights reserved.
//

import UIKit

class APIModelClass: NSObject {
    
    //MARK: - Api Call for getWeatherFromApi
    
    func getWeatherFromApi(url: String, complition:  @escaping (Data,Bool)->())  {
        guard let url = URL(string: url) else { return}
        URLSession.shared.dataTask(with: url) { (data, response, error) in
            guard let data = data else{return}
            print(error?.localizedDescription ?? " ")
            complition(data ,true)
            }.resume()
        
    }
}
