//
//  APIViewModel.swift
//  WeatherForecastDemo
//
//  Created by Mac  on 09/10/18.
//  Copyright © 2018 SaritBahuguna. All rights reserved.
//

import UIKit

class APIViewModel: NSObject {
    private var abc : Json4Swift_Base!
    
    //MARK: - Api Call for getWeatherFromApi with business logic

    func getWeatherForeCast(url: String, complition: @escaping (String,Bool,Json4Swift_Base)->())  {
        APIModelClass().getWeatherFromApi(url: url) { (data, message) in
            if message{
                do{
                    self.abc =  try JSONDecoder().decode(Json4Swift_Base.self, from: data)
                    print(self.abc)
                    complition("data parsed successfully",true,self.abc)
                }
                catch{
                    complition("failed to parse the data", false,self.abc)
                    return
                }
            }
            else{
                complition("failed to parse the daqta", false,self.abc)
            }
            
        }
    }
    
}
