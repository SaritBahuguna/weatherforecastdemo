//
//  WeatherTableViewCell.swift
//  WeatherForecastDemo
//
//  Created by Mac  on 10/10/18.
//  Copyright © 2018 SaritBahuguna. All rights reserved.
//

import UIKit

class WeatherTableViewCell: UITableViewCell {

    @IBOutlet weak var tempLabel: UILabel!
    @IBOutlet weak var date_timeLabel: UILabel!
    @IBOutlet weak var img_weatherImg: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    

}
