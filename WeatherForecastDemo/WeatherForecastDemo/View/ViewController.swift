//
//  ViewController.swift
//  WeatherForecastDemo
//
//  Created by Mac  on 10/10/18.
//  Copyright © 2018 SaritBahuguna. All rights reserved.
//

import UIKit
class ViewController: BaseViewController, TableViewDelegate {
    
    @IBOutlet weak var listTabView: UITableView!
    private var weatherCast: [List] = []
    
    private var cityDetail: City?
    private var mainDetail: Main?
    
    //MARK: -  Show Table View Delegate and Data Source Methods
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return weatherCast.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell: WeatherTableViewCell = self.listTabView.dequeueReusableCell(withIdentifier: "customCell") as! WeatherTableViewCell
        cell.date_timeLabel?.font  = UIFont(name: "Oswald-Bold", size: 20.0)
        cell.tempLabel?.font  = UIFont(name: "Oswald-Bold", size: 12.0)
        
        
        cell.date_timeLabel?.text = String(format: "%@", (weatherCast[indexPath.row].dt_txt)!)
        cell.tempLabel?.text = String(format: "%@", (weatherCast[indexPath.row].weather![0].weatherDescription)!)
        
        return cell
    }
    
    public func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat{
        return 92.0
    }
    
    
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        let weatherDetail = DetailModel(cityDetail: cityDetail, listDetail: weatherCast[indexPath.row])
        self.push(controllerWithIdentifier: DeatilWeatherViewController.identifier, pass: weatherDetail)
        
    }
    
    //MARK: -  View Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "List of weather forecast"
        getWeather_byCity()
    }
    
    //MARK: -  Api Call using City
    
    func getWeather_byCity(){
        LoadingHUD.showLoadingHUD(to_view: self.view)
       
        let urlString = "https://samples.openweathermap.org/data/2.5/forecast?q=delhi,IN&appid=b6907d289e10d714a6e88b30761fae22"
        
        APIViewModel().getWeatherForeCast(url: urlString) {(str, message,response) in
            if message{
                DispatchQueue.main.async {
                    LoadingHUD.hideLoadingHUD(for_view: self.view)

                    self.listTabView.dataSource = self
                    self.listTabView.delegate = self
                    self.cityDetail = response.city
                    self.weatherCast = (response.list! as NSArray) as! [List]
                    self.listTabView.reloadData()
                }
            }
            else{
                DispatchQueue.main.async {
                    LoadingHUD.hideLoadingHUD(for_view: self.view)

                self.showAlert(title: "Error", message: "Something went wrong")
                }
            }
        }
    }
    
    
}

