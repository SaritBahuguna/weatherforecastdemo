//
//  BaseViewController.swift
//  WeatherForecastDemo
//
//  Created by Mac  on 10/10/18.
//  Copyright © 2018 SaritBahuguna. All rights reserved.
//

import UIKit

class BaseViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

    }
    
    static var identifier: String {
        return String(describing: self)
    }
    
    func showAlert(title: String = "", message: String) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alertController.view.tintColor = UIColor.red
        
        let okAction = UIAlertAction(title: "OK", style: .default)
        
        alertController.addAction(okAction)
        self.present(alertController, animated: true, completion: nil)
    }

}

//MARK: create extension for push view controller
extension BaseViewController {
    func push(controllerWithIdentifier identifier: String, pass data: DetailModel) {
        
        guard let controller = self.storyboard?.instantiateViewController(withIdentifier: identifier) as? DeatilWeatherViewController else {return}
        controller.weatherDetails = data
        self.navigationController?.pushViewController(controller, animated: true)
        
        
    }
}
