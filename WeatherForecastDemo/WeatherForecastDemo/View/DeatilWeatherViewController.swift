//
//  DeatilWeatherViewController.swift
//  WeatherForecastDemo
//
//  Created by Mac  on 10/10/18.
//  Copyright © 2018 SaritBahuguna. All rights reserved.
//

import UIKit

class DeatilWeatherViewController: BaseViewController {
    
    var weatherDetails: DetailModel?
    
    @IBOutlet weak var placeNameLabel: UILabel!
    @IBOutlet weak var temperatureLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var minimumTempLabel: UILabel!
    @IBOutlet weak var maximumTempLabel: UILabel!
    @IBOutlet weak var windLabel: UILabel!
    @IBOutlet weak var pressureLabel: UILabel!
    @IBOutlet weak var forecastDescLabel: UILabel!
    
    //MARK: - View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.updateUIWithData()
    }
    
    private func updateUIWithData() {
        guard let detail = weatherDetails else {return}
        self.placeNameLabel.text = detail.cityDetail?.name
        self.temperatureLabel.text = String(detail.listDetail?.main?.temp ?? 0)
        self.dateLabel.text = detail.listDetail?.dt_txt
        self.minimumTempLabel.text = String(detail.listDetail?.main?.temp_min ?? 0)
        self.maximumTempLabel.text = String(detail.listDetail?.main?.temp_max ?? 0)
        self.windLabel.text = self.join(doubleValue: detail.listDetail?.wind?.speed,
                                        with: "m/s")
        self.pressureLabel.text = self.join(doubleValue: detail.listDetail?.main?.pressure,
                                            with: "hPa")
        self.forecastDescLabel.text = detail.listDetail?.weather?[0].weatherDescription
    }
    
    private func join(doubleValue: Double?, with unit: String) -> String {
        guard let doubleValue = doubleValue else {
            return "0 \(unit)"
        }
        let obj = "\(doubleValue) \(unit)"
        return obj
    }
}
