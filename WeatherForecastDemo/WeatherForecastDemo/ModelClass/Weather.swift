//
//  AppDelegate.swift
//  WeatherForecastDemo
//
//  Created by Mac  on 09/10/18.
//  Copyright © 2018 SaritBahuguna. All rights reserved.
//

import Foundation
struct Weather : Codable {
	let id : Int?
	let main : String?
	let weatherDescription : String?
	let icon : String?

	enum CodingKeys: String, CodingKey {

		case id = "id"
		case main = "main"
		case weatherDescription = "description"
		case icon = "icon"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		id = try values.decodeIfPresent(Int.self, forKey: .id)
		main = try values.decodeIfPresent(String.self, forKey: .main)
		weatherDescription = try values.decodeIfPresent(String.self, forKey: .weatherDescription)
		icon = try values.decodeIfPresent(String.self, forKey: .icon)
	}

}
