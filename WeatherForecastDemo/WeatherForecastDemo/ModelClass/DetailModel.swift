//
//  DetailModel.swift
//  WeatherForecastDemo
//
//  Created by Mac  on 10/10/18.
//  Copyright © 2018 SaritBahuguna. All rights reserved.
//

import Foundation

struct DetailModel {
    var cityDetail: City?
    var listDetail: List?
}
