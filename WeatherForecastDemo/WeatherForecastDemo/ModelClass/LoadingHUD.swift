//
//  LoadingHUD.swift
//  TRADE
//
//  Created by Parvendra Kumar on 02/07/18.
//  Copyright © 2018 Parvendra Singh. All rights reserved.
//

import UIKit
import MBProgressHUD

class LoadingHUD: NSObject {
    
    // Show HUD
    static func showLoadingHUD(to_view: UIView) {
        let hud = MBProgressHUD.showAdded(to: to_view, animated: true)
        hud.mode = .indeterminate
    }
    
    // Hide HUD
    
    static func hideLoadingHUD(for_view: UIView) {
        MBProgressHUD.hide(for: for_view, animated: true)
    }

}
