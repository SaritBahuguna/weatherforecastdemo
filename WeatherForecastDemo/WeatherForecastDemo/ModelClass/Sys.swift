//
//  AppDelegate.swift
//  WeatherForecastDemo
//
//  Created by Mac  on 09/10/18.
//  Copyright © 2018 SaritBahuguna. All rights reserved.
//

import Foundation
struct Sys : Codable {
	let pod : String?

	enum CodingKeys: String, CodingKey {

		case pod = "pod"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		pod = try values.decodeIfPresent(String.self, forKey: .pod)
	}

}
